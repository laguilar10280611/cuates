<%-- 
    Document   : index
    Created on : 12/10/2014, 07:14:31 PM
    Author     : Armand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BD CON JSP</title>
    </head>
    <body>
        
        
        <jsp:useBean id="usuario" class="armando.User" scope="request">
            <jsp:setProperty name="usuario" property="*"/>
        </jsp:useBean>
        <%@include file="/WEB-INF/conexion.jspf"%>
        
        <sql:transaction dataSource="${conexion}">
            
            <c:catch var="ex">
                <sql:query var="resultado">
                    SELECT  nombre FROM cuates where nombre='<jsp:getProperty name="usuario" property="nombre"/>' AND password='<jsp:getProperty name="usuario" property="password"/>';
                </sql:query>
           </c:catch>
           <c:choose>
                <c:when test="${not empty ex}">
                 <p>Problema: <c:out value="${ex.message}"/></p>
                </c:when>
                <%-- Si no hubo ninguna excepción --%>
                <c:otherwise>
                    
                    <c:choose>
                        <%-- Si encontró algo --%>
                        <c:when test="${resultado.rowCount>0}">
                         <ol>
                             <h1>BIENVENIDO A LA MORADA DE CUATES</h1><hr>
                             <h1> TE HAS IDENTIFICADO COMO: <jsp:getProperty name="usuario" property="nombre"/></h1>
                         </ol>
                        </c:when>
                        <c:otherwise>
                         <p>ALERTA DE INTRUSO</p><hr>
                         <h1>LLAMEN A LOS PERROS...!!!</h1>
                        </c:otherwise>
                    </c:choose>

                </c:otherwise>
            </c:choose> 
        </sql:transaction> 
    </body>
</html>
